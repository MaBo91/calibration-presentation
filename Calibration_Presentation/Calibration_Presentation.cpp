#include <GL\glut.h>
#include <cmath>
#include <iostream>

GLfloat WHITE[] = { 1, 1, 1 };
GLfloat BLACK[] = { 0, 0, 0 };

class Position {
	// Eye Positions
	double x, y, z;
	// Normal vector of eye position
	bool upVectorX;
	bool upVectorY;
	bool upVectorZ;

public:
	Position() : x(4.025), y(20.5), z(4), upVectorX(false), upVectorY(false), upVectorZ(true) {}
	double getEyeX() { return x; }
	double getEyeY() { return y; }
	double getEyeZ() { return z; }

	bool getCameraUpX() { return upVectorX; }
	bool getCameraUpY() { return upVectorY; }
	bool getCameraUpZ() { return upVectorZ; }

	void moveEyeX()
	{
		double maxvalue = 10; // Value-Obergrenze
		double minvalue = -10; // Value-Untergrenze
		x = (maxvalue - minvalue) * ((double)rand() / (double)RAND_MAX) + minvalue;
	}

	void moveEyeY()
	{
		double maxvalue = 31;
		double minvalue = 30;
		y = (maxvalue - minvalue) * ((double)rand() / (double)RAND_MAX) + minvalue;
	}

	void moveEyeZ()
	{
		double maxvalue = 10;
		double minvalue = -10;
		z = (maxvalue - minvalue) * ((double)rand() / (double)RAND_MAX) + minvalue;
	}
};

// Checkerboard
class Checkerboard {
	int displayListId;
	int width;
	int depth;
	double x = 0, z = 0;
public:
	Checkerboard(int width, int depth) : width(width), depth(depth) {}
	double getCenter_offsetX() { return x; }
	double getCenter_offsetZ() { return z; }

	double center_offsetX()
	{
		double maxvalue = 14; // Value-Obergrenze
		double minvalue = -7; // Value-Untergrenze
		x = (maxvalue - minvalue) * ((double)rand() / (double)RAND_MAX) + minvalue;
		return (width / 2) + x;
	}

	double center_offsetZ()
	{
		double maxvalue = 10; // Value-Obergrenze
		double minvalue = -3; // Value-Untergrenze
		z = (maxvalue - minvalue) * ((double)rand() / (double)RAND_MAX) + minvalue;
		return (depth / 2) + z;
	}

	void create() {
		displayListId = glGenLists(1);
		glNewList(displayListId, GL_COMPILE);
		glBegin(GL_QUADS);
		glNormal3d(0, 1, 0);
		for (int x = 0; x < width - 1; x++) {
			for (int z = 0; z < depth - 1; z++) {
				glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, (x + z) % 2 == 0 ? BLACK : WHITE); // GL_AMBIENT_AND_DIFFUSE
				glVertex3d(x, 0, z);
				glVertex3d(x + 1, 0, z);
				glVertex3d(x + 1, 0, z + 1);
				glVertex3d(x, 0, z + 1);
			}
		}
		glEnd();
		glEndList();
	}

	void draw() {
		glCallList(displayListId);
	}
};

Checkerboard checkerboard(8, 8);
Position position;

void init() {
	glEnable(GL_DEPTH_TEST);
	glLightfv(GL_LIGHT0, GL_AMBIENT, WHITE); // GL_DIFFUSE
	glLightfv(GL_LIGHT0, GL_AMBIENT, WHITE); // GL_SPECULAR
	glMaterialfv(GL_FRONT, GL_AMBIENT, WHITE); // GL_SPECULAR
	glMaterialf(GL_FRONT, GL_AMBIENT, 30); // GL_SHINIESS
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glShadeModel(GL_SMOOTH); //GL_SMOOTH
	checkerboard.create();
}

void display()
{
	glClearColor(1, 1, 1, 1); // White Background
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	gluLookAt(position.getEyeX(), position.getEyeY(), position.getEyeZ(), checkerboard.getCenter_offsetX(), 0, checkerboard.getCenter_offsetZ(), position.getCameraUpX(), position.getCameraUpY(), position.getCameraUpZ());
	checkerboard.draw();
	std::cout << "Eye Position" << std::endl;
	std::cout << "X: " << position.getEyeX() << std::endl << "Y: " << position.getEyeY() << std::endl << "Z: " << position.getEyeZ() << std::endl << std::endl;
	std::cout << "Lookat Position" << std::endl;
	std::cout << "X: " << checkerboard.getCenter_offsetX() << std::endl << "Z: " << checkerboard.getCenter_offsetZ() << std::endl << std::endl;
	glFlush();
	glutSwapBuffers();
}

// Construct a camera that perfectly fits the window
void reshape(GLint w, GLint h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(40.0, GLfloat(w) / GLfloat(h), 1.0, 150.0); //40.0, ..., 1.0, 150.0
	glMatrixMode(GL_MODELVIEW);
}

// To draw the next Frame
void timer(int v)
{
	glutPostRedisplay();
	glutTimerFunc(1000 / 60, timer, v);   
}

// Perform random movement of eye + view position
void movePositions()
{
	int random;
	for (int pos = 0; pos < 100; pos++)
	{
		random = rand() % (2 + 1); // Rand between 0, 1 and 2
		Sleep(9); // Wait for displaying next frame
		switch (random) {
		case 0: position.moveEyeX();  break;
		case 1: position.moveEyeY();  break;
		case 2: position.moveEyeZ();  break;
		}
		checkerboard.center_offsetX();
		checkerboard.center_offsetZ();
		glutPostRedisplay();
	}
}

void main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(1920, 1080); // HD-Ready:1280x720  Full-HD: 1920x1080
	glutCreateWindow("Checkerboard Presentation | OpenGL Window");
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutIdleFunc(movePositions);
	glutTimerFunc(100, timer, 0);
	init();
	glutMainLoop();
}